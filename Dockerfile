FROM openjdk:8-jdk-alpine

COPY ./target/book-management-1.0.jar /usr/app/

WORKDIR /usr/app

RUN sh -c 'touch book-management-1.0.jar'

ENTRYPOINT ["java","-jar","book-management-1.0.jar"]
