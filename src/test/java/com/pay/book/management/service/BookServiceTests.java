package com.pay.book.management.service;

import com.pay.book.management.domain.Book;
import com.pay.book.management.domain.BookType;
import com.pay.book.management.dto.BookDto;
import com.pay.book.management.exceptionManager.ExceptionMessages;
import com.pay.book.management.exceptionManager.NoContentExceptionHandler;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class BookServiceTests {

    @Autowired
    private BookService bookService;

    private BookDto bookDto;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    public BookDto bookInfo() {
        bookDto = new BookDto();
        bookDto.setName("The Art Of War");
        bookDto.setIsbnNumber("978-3-16-148410-0");
        bookDto.setPrice(100);
        bookDto.setPublishDate("2022-03-03");
        bookDto.setBookType(BookType.E_BOOK);
        System.out.println(bookDto.getPublishDate());
        return bookDto;
    }

    @Test
    public void processBookInfo() {
        Book book = bookService.processBookInfo(new Book(), bookInfo());
        Assert.assertNotNull(book);
        Assert.assertEquals(bookDto.getName(), book.getName());
        Assert.assertEquals(bookDto.getIsbnNumber(), book.getIsbnNumber());
        Assert.assertEquals(bookDto.getPrice(), book.getPrice(), 0.0);
        Assert.assertEquals(bookDto.getPublishDate(), book.getPublishDate());
        Assert.assertEquals(bookDto.getBookType(), book.getBookType());
    }

    @Test
    public void createBook() {
        bookService.createBook(bookInfo());
    }

    @Test
    public void updateBookInfo() {
        bookService.createBook(bookInfo());
        List<BookDto> bookList = bookService.findAllBooks();
        if (bookList.size() > 0) {
            BookDto bookDto = bookList.get(0);
            bookDto.setName("Art Of War ||");
            bookDto.setPrice(120);
            bookService.updateBookInfo(bookList.get(0));
        }
    }

    @Test(expected = NoContentExceptionHandler.class)
    public void findAllBooks_empty() {
        bookService.findAllBooks();
    }

    @Test
    public void deleteBook_notfound() {
        exceptionRule.expect(IllegalArgumentException.class);
        exceptionRule.expectMessage(ExceptionMessages.BOOK_NOT_FOUND);
        bookService.deleteBook(2L);
    }
}
