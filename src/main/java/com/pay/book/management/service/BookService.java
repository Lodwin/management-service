package com.pay.book.management.service;

import com.pay.book.management.domain.Book;
import com.pay.book.management.dto.BookDto;
import com.pay.book.management.exceptionManager.ExceptionMessages;
import com.pay.book.management.exceptionManager.NoContentExceptionHandler;
import com.pay.book.management.repository.BookRepository;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Log
public class BookService implements IBookService {
    @Autowired
    private BookRepository bookRepository;

    @Override
    public void createBook(BookDto bookDto) {
        Book book = new Book();
        bookRepository.saveAndFlush(processBookInfo(book, bookDto));
    }

    @Override
    public void updateBookInfo(BookDto bookDto) {
        if (findBookByID(bookDto.getBookID()).isPresent()) {
            bookRepository.saveAndFlush(processBookInfo(
                    findBookByID(bookDto.getBookID()).get(),
                    bookDto));
        } else {
            throw new IllegalArgumentException(ExceptionMessages.BOOK_NOT_FOUND);
        }
    }

    @Override
    public List<BookDto> findAllBooks() {
        List<Book> books = bookRepository.findAll();
        List<BookDto> bookList = new ArrayList<>();
        if (!books.isEmpty()) {
            books.forEach(book -> {
                BookDto bookDto = new BookDto();
                bookDto.setBookID(book.getBookId());
                bookDto.setName(book.getName());
                bookDto.setIsbnNumber(book.getIsbnNumber());
                bookDto.setPublishDate(book.getPublishDate());
                bookDto.setPrice(book.getPrice());
                bookDto.setBookType(book.getBookType());
                bookList.add(bookDto);
            });
        } else {
            log.warning(ExceptionMessages.NO_CONTENT);
            throw new NoContentExceptionHandler();
        }
        return bookList;
    }

    @Override
    public void deleteBook(Long bookID) {
        if (findBookByID(bookID).isPresent()) {
            bookRepository.deleteById(bookID);
        } else {
            throw new IllegalArgumentException(ExceptionMessages.BOOK_NOT_FOUND);
        }
    }

    /**
     * This method calls the repo to find a book using book ISBN
     * And throws an exception if the book is not found.
     */
    private Optional<Book> findBookByID(Long bookID) {
        return bookRepository.findById(bookID);
    }

    /**
     * This method maps BookDto data to the Book entity
     */
    public Book processBookInfo(Book book, BookDto bookDto) {
        book.setName(bookDto.getName());
        book.setIsbnNumber(bookDto.getIsbnNumber());
        book.setPublishDate(bookDto.getPublishDate());
        book.setPrice(bookDto.getPrice());
        book.setBookType(bookDto.getBookType());
        return book;
    }
}
