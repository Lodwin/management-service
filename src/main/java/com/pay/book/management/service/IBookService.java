package com.pay.book.management.service;

import com.pay.book.management.dto.BookDto;

import java.util.List;

/**
 * This interface consist of methods used to perform CRUD operations
 */
public interface IBookService {
    void createBook(BookDto bookDto);

    void updateBookInfo(BookDto bookDto);

    void deleteBook(Long bookID);

    List<BookDto> findAllBooks();
}
