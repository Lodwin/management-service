package com.pay.book.management.resource;

import com.pay.book.management.dto.BookDto;
import com.pay.book.management.service.IBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Book management application endpoints
 */
@RestController
@RequestMapping("/management")
public class BookResource {

    @Autowired
    private IBookService bookService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<BookDto> books() {
        return bookService.findAllBooks();
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String createBook(@RequestBody BookDto bookDto) {
        bookService.createBook(bookDto);
        return HttpStatus.CREATED.name();
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String updateBookInfo(@RequestBody BookDto bookDto) {
        bookService.updateBookInfo(bookDto);
        return "Updated Successfully";
    }

    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    public String deleteBook(@PathVariable Long id) {
        bookService.deleteBook(id);
        return "Deleted Successfully ";
    }
}
