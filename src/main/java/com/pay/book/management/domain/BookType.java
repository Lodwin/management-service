package com.pay.book.management.domain;


/**
 * Types of Books.
 */
public enum BookType {
    HARD_COVER,
    SOFT_COVER,
    E_BOOK
}
