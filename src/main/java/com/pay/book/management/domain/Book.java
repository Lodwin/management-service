package com.pay.book.management.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * Lombok Simple Configs for Getters and Setter including a Not Arg Constructor
 */
@NoArgsConstructor
@Setter
@Getter
/**
 * JPA Specification to specify the table to map to.
 */
@Entity
@Table(name = "Book")
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "BookID", nullable = false, unique = true, updatable = false)
    private Long bookId;

    @Column(name = "Name", length = 100)
    private String name;

    @Column(name = "ISBN", length = 50)
    private String isbnNumber;

    @Column(name = "PublishDate", length = 50)
    private String publishDate;

    @Column(name = "Price")
    private double price;

    @Enumerated(EnumType.STRING)
    @Column(name = "BookType", length = 50)
    private BookType bookType;
}
