package com.pay.book.management.exceptionManager;

/**
 * Just a collection of my custom Exception Messages.
 */
public interface ExceptionMessages {
    String BOOK_NOT_FOUND = "Book Not Found, please verify";
    String DUPLICATE = "Book Already Exits";
    String NO_CONTENT = "No Books Found";
}
