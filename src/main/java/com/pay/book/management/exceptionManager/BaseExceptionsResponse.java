package com.pay.book.management.exceptionManager;

import lombok.Getter;
import lombok.Setter;

/**
 * Lombok Configs to work on Getters & Setters
 */
@Getter
@Setter
public class BaseExceptionsResponse
{
    private String errorType;
    private String message;

    /**
     * Builds exception response body or message
     * @param errorType http status
     * @param message exception details
     * @return
     */
    public static BaseExceptionsResponse of(String errorType, String message) {
        BaseExceptionsResponse baseExceptionsResponse = new BaseExceptionsResponse();
        baseExceptionsResponse.setErrorType( errorType);
        baseExceptionsResponse.setMessage( message);
        return baseExceptionsResponse;
    }
}
