package com.pay.book.management.exceptionManager;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler implements ExceptionMessages {

    /**
     * Exception Handler responsible for service illegal exceptions
     * @param exception
     * @param request
     * @return
     */
    @ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class,
            ConstraintViolationException.class})
    protected ResponseEntity<Object> handleConflict( Exception exception, WebRequest request) {
        BaseExceptionsResponse error = BaseExceptionsResponse.of(HttpStatus.NOT_FOUND.name(), BOOK_NOT_FOUND);
        HttpHeaders headers = withHeaders();

        return handleExceptionInternal(exception, error, headers, HttpStatus.NOT_FOUND, request);
    }

    /**
     * Exception Handler responsible for service not found exceptions
     * @param exception
     * @param request
     * @return
     */
    @ExceptionHandler(value = {NoContentExceptionHandler.class})
    protected ResponseEntity<Object> notFoundExceptions( Exception exception, WebRequest request) {
        BaseExceptionsResponse error = BaseExceptionsResponse.of(HttpStatus.NO_CONTENT.name(), NO_CONTENT);
        HttpHeaders headers = withHeaders();

        return handleExceptionInternal(exception, error, headers, HttpStatus.NO_CONTENT, request);
    }

    private static HttpHeaders withHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }

}
