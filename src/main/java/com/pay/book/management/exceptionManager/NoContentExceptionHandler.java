package com.pay.book.management.exceptionManager;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT, reason = ExceptionMessages.NO_CONTENT)
public class NoContentExceptionHandler extends RuntimeException {}
