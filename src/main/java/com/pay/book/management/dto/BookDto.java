package com.pay.book.management.dto;

import com.pay.book.management.domain.BookType;
import lombok.Data;

/**
 * Lombok to specify that this is a data class.
 * It is mainly going to be used as a REST Data Model.
 */
@Data
public class BookDto {
    private Long bookID;
    private String name;
    private String isbnNumber;
    // No formatting for publishDate because we will always receive {dd/MM/yyyy} format
    // from the ui service
    private String publishDate;
    private double price;
    private BookType bookType;
}
