# Book-Management Service
Book-Management Service is a service that manages books catalogue.
# Build Tool
- ##### Maven
# DevOps
- ##### Docker
- ##### Git
# Frameworks
#### Spring Boot
- Service build framework.
- Service Endpoint Url (http://localhost:8080/book-management).
#### Spring Actuator
- Used for service **monitoring**.
    - All components - Url (http://localhost:8080/manage-book-management).
    - Specific components - Url (http://localhost:8080/manage-book-management/*).
        - ###### Health check
            - Url (`*/health`)
        - ###### Information
            - Url (`*/info`)
        - ###### Metrics
            - Url (`*/metrics`)
#### Spring Controller Advice
- Used for exception handling strategy.
#### Lombok
- Java Library used for reducing boiler plate code such as getters and setters etc.
#### Logging
- Used log4j2 for generating log files.
    - Path (`book-management/logs`).
# Run Instructions
- Please have Docker, Java Compatible IDE (e.g IntelliJ) installed on your machine.
- JDK 1.8 or newer, but note that if you use newer java versions will as well need to change docker JDK in the dockerfile to avoid exceptions.
- Clone service
    - Url (https://Lodwin@bitbucket.org/Lodwin/management-service.git)
- Run maven refresh to import all dependencies.
- Run maven [clean, install] goals to build service jar.
# Service Execution with executable jar
- Execute script (`./executeJar.sh`) that will execute book-management jar
# Dockerization & Deployment
- Please once again verify
    - that your compile JDK and dockerfile JDK are the same versions to avoid exceptions.
    - execute docker commands from application root.
    - (`book-management-1.0.jar`) is available in the target folder
- ###### Build Image
    - Execute docker command - ( `docker build -t book-management . `).
- ###### Run & port binding
    - Execute docker command - (`docker run -p 8080:8080 book-management`).
- ###### Image name
    - book-management
# Database Information
- H2 In-Memory Database
- url (`*/h2-console`)
- Credentials (please get credentials from `application.properties`)
# Future Improvements
- Service security layer implementation such that only authorized clients can consume the service.
    - ###### Security Mechanism
        - JWT
        - Spring Security
- Field validation, error handling, and test coverage

